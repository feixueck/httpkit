package com0oky.httpkit.test;

import java.util.Map;

import org.junit.Test;

import com0oky.httpkit.http.HttpKit;
import com0oky.httpkit.http.ResponseWrap;
import com0oky.httpkit.http.request.RequestBase;

/**
 * 使用Object对象作为请求参数
 * @author mdc
 * @date 2018年1月30日
 */
public class ObjectParamTest {

	public static class OscUserInfo {
		
		private String email;
		private String pwd;
		private String verifyCode = "";
		private int save_login;
		/**
		 * 获取{@linkplain #email}
		 * @return email
		 */
		public String getEmail() {
			return email;
		}
		/**
		 * 获取{@linkplain #pwd}
		 * @return pwd
		 */
		public String getPwd() {
			return pwd;
		}
		/**
		 * 获取{@linkplain #verifyCode}
		 * @return verifyCode
		 */
		public String getVerifyCode() {
			return verifyCode;
		}
		/**
		 * 获取{@linkplain #save_login}
		 * @return save_login
		 */
		public int getSave_login() {
			return save_login;
		}
		/**
		 * 设置{@linkplain #email}
		 * @param email email
		 */
		public void setEmail(String email) {
			this.email = email;
		}
		/**
		 * 设置{@linkplain #pwd}
		 * @param pwd pwd
		 */
		public void setPwd(String pwd) {
			this.pwd = Sha1Util.getSha1(pwd);
		}
		/**
		 * 设置{@linkplain #verifyCode}
		 * @param verifyCode verifyCode
		 */
		public void setVerifyCode(String verifyCode) {
			this.verifyCode = verifyCode;
		}
		/**
		 * 设置{@linkplain #save_login}
		 * @param save_login save_login
		 */
		public void setSave_login(int save_login) {
			this.save_login = save_login;
		}
	}
	
	@Test
	public void userObjectParamsTest(){
		String url = "https://www.oschina.net/action/user/hash_login";
		String userAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36";
		
		RequestBase request = HttpKit.post(url);
		
		OscUserInfo info = new OscUserInfo();
		info.setEmail("email");
		info.setPwd("***");
		info.setSave_login(1);
		
		request.setParameter(info)
		.addHeader("Origin", "https://www.oschina.net")
		.setUserAgent(userAgent);
		
		ResponseWrap response = request.execute();
		
		String resultString = response.getString();
		if (resultString != null && resultString.trim().length() > 0) {
			Map<?, ?> json = response.getJson(Map.class);
			System.out.println("HttpKit登录失败===>" + json.get("msg"));
			return ;
		}
		
		System.out.println(response.getString());
		System.out.println("登录成功===>" + response.getStatusLine());
		
		//跳转到一个页面, 保持Session, 只需要把登录的HttpKit传到下一次请求即可
		String html = HttpKit.get("https://my.oschina.net/yiq", request)
		.setUserAgent(userAgent)
		.execute().getString();
		
		String userStr = "owner_id\" data-value=\"";
		int dataUserIndex = html.indexOf(userStr) + userStr.length();
		
		//获取到UserId
		String userId = html.substring(dataUserIndex, html.indexOf("\">", dataUserIndex));
		
		System.out.println("获取到UserId===>" + userId);
	}
}
